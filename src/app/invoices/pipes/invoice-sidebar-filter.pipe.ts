import { Pipe, PipeTransform } from '@angular/core';
import { AnyCatcher } from 'rxjs/internal/AnyCatcher';
import { InvoiceDocument } from '../services/invoice-document';

@Pipe({
  name: 'invoiceSidebarFilter'
})
export class InvoiceSidebarFilterPipe implements PipeTransform {

  transform(items: any[], searchText:string): any[] {

    if (!items) {
      return [];
    }

    if (!searchText) {
      return items;
    }

    searchText = searchText.toLocaleLowerCase();

    return items.filter(it => {

      return it.number.toLocaleLowerCase().includes(searchText);
    });

  }

}
