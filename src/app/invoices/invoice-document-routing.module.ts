import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from '../helpers/auth.gaurd';
import { InvoiceDetailContentComponent } from './invoice-detail-content/invoice-content.component';
import { InvoicesMainPageComponent } from './invoices-main-page.component';


const routes: Routes = [
  { path: '', redirectTo: '/invoices/new', pathMatch: 'full' },
  { path: 'main', component: InvoicesMainPageComponent },
  {
    path: 'invoices', component: InvoicesMainPageComponent, children: [
      { path: 'new', component: InvoiceDetailContentComponent , canActivate: [AuthGuard]},
      { path: ':id', component: InvoiceDetailContentComponent , canActivate: [AuthGuard]}
    ]
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class InvoiceDocumnetRoutingModule { }
