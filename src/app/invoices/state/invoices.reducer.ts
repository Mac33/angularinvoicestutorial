import { createReducer,on } from "@ngrx/store";
import { InvoiceActions } from './actions';
import * as AppState from '../../state/app.state';


export interface InvoicesState
{
  filterByPayStatus:string,
  searchTerm:string,
  lastInvoiceNumber:number,
}

export interface State extends AppState.State
{
  invoices:InvoicesState
}


export const initialState: InvoicesState = {
  filterByPayStatus: 'all',
  searchTerm: '',
  lastInvoiceNumber: -1,
};


export const invoicesReducer = createReducer<InvoicesState>(
  initialState,
  on(InvoiceActions.setPaymentFilter, (state, action):InvoicesState=> {
    let newState :InvoicesState = {
      ...state,
      filterByPayStatus:action.filterByPayStatus
    };
    return newState;
  } ),
  on(InvoiceActions.setSearchFilter, (state, action):InvoicesState=> {
    let newState :InvoicesState = {
      ...state,
      searchTerm:action.searchTerm
    };
    return newState;
  }),
  on(InvoiceActions.setLastInvoce, (state, action):InvoicesState=> {
    let newState :InvoicesState = {
      ...state,
      lastInvoiceNumber:action.invoiceNumber
    };
    return newState;
  })
  )
