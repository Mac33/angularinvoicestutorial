import { createAction, props } from "@ngrx/store";


export const setPaymentFilter = createAction('[Invoices] set payment filter', props<{filterByPayStatus: string}>());


export const setSearchFilter = createAction('[Invoices] set search filter', props<{searchTerm: string}>());

export const setLastInvoce = createAction('[Invoices] set last invoice number', props<{invoiceNumber: number}>());
