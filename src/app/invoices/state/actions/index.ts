import { createFeatureSelector } from '@ngrx/store';
import { InvoicesState } from '../invoices.reducer';
import * as InvoiceActions from './invoices.actions';

export { InvoiceActions };


export const getInvoceSelector =  createFeatureSelector<InvoicesState>('invoices')
