import { ComponentFixture, TestBed } from '@angular/core/testing';

import { InvoiceDetailContentComponent } from './invoice-content.component';

describe('InvoiceContentComponent', () => {
  let component: InvoiceDetailContentComponent;
  let fixture: ComponentFixture<InvoiceDetailContentComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ InvoiceDetailContentComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(InvoiceDetailContentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
