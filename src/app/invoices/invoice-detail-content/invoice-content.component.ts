import { ActivatedRoute, Router } from '@angular/router';
import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { UntypedFormBuilder, UntypedFormGroup, Validators } from '@angular/forms';

import { BehaviorSubject, Subscription } from 'rxjs';

import { InvoiceDocument } from '../services/invoice-document';
import { InvoicesService } from '../services/invoices.service';
import { InvoiceDocumentItem } from '../services/invoice-document-item'
import { roundTo } from 'src/app/helpers/round';
import { select, Store } from '@ngrx/store';
import { StoreRootState } from 'src/app/state/store-root.state';
import { getCurrentRouteState } from 'src/app/state/store-root.selector';
import { RouterStateUrl } from 'src/app/helpers/router-custom-serializer';

@Component({
  selector: 'app-invoice-content',
  templateUrl: './invoice-content.component.html',
  styleUrls: ['./invoice-content.component.scss']
})
export class InvoiceDetailContentComponent implements OnInit, OnDestroy {

  @Input() searchTerms!: BehaviorSubject<string>;

  get invoiceNumber() {
    let control = this.invoiceForm.get('number');
    if (control == null) {
      return undefined;
    }
    return control;
  }

  get invoicePartnerName() {
    return this.invoiceForm.get('partnerName') || undefined;
  }

  invoiceForm: UntypedFormGroup;
  isNewInvoice = true;
  showPanel = true;


  constructor(
    private invoiceService: InvoicesService,
    private route: ActivatedRoute,
    private router: Router,
    private store: Store<StoreRootState>,
    fb: UntypedFormBuilder,
  ) {

    this.invoiceForm = fb.group({
      invoiceNumber: ["", Validators.required],
      dateTime: new Date(Date.now()).toISOString().slice(0, 10),
      isPaid: false,
      totalSum: [{ value: 0, disabled: true }],
      partnerName: ["", Validators.required],
      id: [{ value: 0, disabled: true }],
      items: [],
    });


    this.invoiceForm.get("items")?.valueChanges.subscribe(
      (value) => {
        this.recalculateTotalSum(value);
      }
    );

  }
  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }


  private subscription!: Subscription;

  ngOnInit(): void {

    this.subscription = this.store
      .pipe(select(getCurrentRouteState))
      .subscribe((route: any) => {

        let id = (route as RouterStateUrl).id;
        if (id > 0) {
          this.bindInvoice(id.toString());
        }
      });
  }


  validate(item: string): string {
    let control = this.invoiceForm.get(item);
    if (control?.errors && (control.touched || control.dirty)) {
      return "is-invalid";
    }

    if (control?.valid) {
      return "is-valid";
    }

    return "";
  }



  onNew() {
    this.router.navigateByUrl('/invoices/new');
  }



  onUpdate() {
    let item: InvoiceDocument = { ...this.invoiceForm.getRawValue() };
    this.invoiceService.updateInvoice(item).subscribe();
  }



  onAddInvoice() {
    if (!this.invoiceForm.invalid) {
      let item: InvoiceDocument = { ...this.invoiceForm.getRawValue(), id: Math.ceil(Math.random() * 10000000) };
      this.invoiceService.addInvoice(item).subscribe();
    }
  }



  private bindInvoice(number: string) {

    if (this.router.url === "/invoices/new") {
      this.isNewInvoice = true;
    } else {
      this.isNewInvoice = false;
      this.invoiceService.getInvoice(number).subscribe(
        {
          next: (invoice) => {
            this.invoiceForm.reset()
            if (invoice) {
              this.showPanel = true;
              this.invoiceForm.patchValue(invoice);
              this.invoiceForm.patchValue({
                dateTime: (new Date(invoice.dateTime)).toISOString().slice(0, 10)
              })
            } else {
              this.showPanel = false;
            }
          },

        }
      );
    }
  }



  private recalculateTotalSum(items: InvoiceDocumentItem[]) {
    let totalSum: number = 0.0;

    if (items) {
      for (let item of items) {
        totalSum = totalSum++ + (item.totalPrice ? item.totalPrice : 0);
      }
    }
    this.invoiceForm.get("totalSum")?.setValue(roundTo(totalSum, 2));
  }

















}
