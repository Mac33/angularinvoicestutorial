import { FnParam } from '@angular/compiler/src/output/output_ast';
import { Component } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { FormArray, UntypedFormBuilder, UntypedFormControl, FormGroup, FormsModule, ReactiveFormsModule, Validators } from '@angular/forms';
import { By } from '@angular/platform-browser';
import { InvoiceDocumentItem } from '../../../services/invoice-document-item';

import { InvoiceDocumentItemComponent } from './invoice-document-item.component';

@Component({
  template: `
      <app-invoice-document-item id="invoiceDocumentControl" [formControl]= "item" ></app-invoice-document-item>`
})
class TestHostComponent {

  item:UntypedFormControl = new UntypedFormControl({
    id: 0,
    itemName: '',
    amount: 0,
    unitPrice: 0,
    totalPrice: 0}) ;

  constructor(private fb: UntypedFormBuilder) {}


  writeValue(item:InvoiceDocumentItem): void {
    this.item = new UntypedFormControl(item)
  }
}


describe('InvoiceDocumentItemComponent', () => {
  let component: TestHostComponent | null = null;
  let fixture: ComponentFixture<TestHostComponent>;
  let mainElement: any;
  let testItem: InvoiceDocumentItem = {
    id: 1,
    itemName: 'TestName A',
    amount: 5,
    unitPrice: 5,
    totalPrice: 25};




  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [
        ReactiveFormsModule,],
      declarations: [
        InvoiceDocumentItemComponent,
        TestHostComponent ]
    })
    .compileComponents();
  });


  beforeEach(() => {
    fixture = TestBed.createComponent(TestHostComponent);
    component = fixture.componentInstance;
    mainElement = fixture.nativeElement.querySelector('app-invoice-document-item');
    fixture.detectChanges();
  });


  fit('should create', () => {
   expect(component).toBeTruthy();
  });

  fit('After binding, component has correct values', () => {

    component = fixture.componentInstance;
    component.writeValue(testItem);
    fixture.detectChanges();

    const itemName =fixture.debugElement.query(By.css('#itemName'));
    expect(itemName.nativeElement.value).toBe(testItem.itemName);

    const amount =fixture.debugElement.query(By.css('#amount'));
    expect(amount.nativeElement.value).toBe(testItem.amount.toString());

    const unitPrice =fixture.debugElement.query(By.css('#unitPrice'));
    expect(unitPrice.nativeElement.value).toBe(testItem.unitPrice.toString());

    const totalPrice =fixture.debugElement.query(By.css('#totalPrice'));
    expect(totalPrice.nativeElement.value).toBe(testItem.totalPrice.toString());

   });


   fit('Recalculate totalPrice after changing of amount value', () => {

    component = fixture.componentInstance;
    component.writeValue(testItem);
    fixture.detectChanges();

    const amount = fixture.debugElement.query(By.css('#amount'));
    amount.nativeElement.value = 10;
    amount.nativeElement.dispatchEvent(new Event('input'));
    fixture.detectChanges();
    const totalPrice =fixture.debugElement.query(By.css('#totalPrice'));
    expect(totalPrice.nativeElement.value).toBe("50");

   });


   fit('Recalculate totalPrice after changing of unit price value', () => {

    component = fixture.componentInstance;
    component.writeValue(testItem);
    fixture.detectChanges();

    const unitPrice = fixture.debugElement.query(By.css('#unitPrice'));
    unitPrice.nativeElement.value = 30;
    unitPrice.nativeElement.dispatchEvent(new Event('input'));
    fixture.detectChanges();
    const totalPrice =fixture.debugElement.query(By.css('#totalPrice'));
    expect(totalPrice.nativeElement.value).toBe("150");

   });




});
