import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { UntypedFormBuilder, UntypedFormGroup, Validators } from '@angular/forms';
import { InvoiceDocumentItem } from '../../../services/invoice-document-item';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';
import { TwoDecimalPlacesPipe } from 'src/app/shared/pipes/two-decimal-places.pipe';


@Component({
  selector: 'app-invoice-document-item',
  templateUrl: './invoice-document-item.component.html',
  styleUrls: ['./invoice-document-item.component.scss'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: InvoiceDocumentItemComponent,
      multi: true
    }]
})
export class InvoiceDocumentItemComponent implements ControlValueAccessor, OnInit {

  item: UntypedFormGroup;

  @Output() removeItem = new EventEmitter<InvoiceDocumentItem>();

  private isRecalculate: boolean = false;

  private changeFn: (value: InvoiceDocumentItem) => void = (value: InvoiceDocumentItem) => { };

  constructor(private fb: UntypedFormBuilder) {

    this.item = this.fb.group({
      id: 0,
      itemName: ['', Validators.required],
      amount: [1, Validators.required],
      unitPrice: [0, Validators.required],
      totalPrice: 0,
    });

  }

  ngOnInit(): void {
    this.item.valueChanges.subscribe(
      (data) => this.recalculate(data)
    )
  }



  writeValue(item: InvoiceDocumentItem): void {
    this.item.setValue(item);
  }



  registerOnChange(fn: (value: InvoiceDocumentItem) => void): void {
    this.changeFn = fn;
  }



  registerOnTouched(fn: any): void { }



  onRemoveItemCliked() {
    this.removeItem.next(this.item.getRawValue());
  }



  private recalculate(data?: any) {
    if (!this.isRecalculate) {
      this.isRecalculate = true;
      let totalPrice =  data.unitPrice * data.amount;
      this.item.controls["totalPrice"].setValue(totalPrice);
      this.isRecalculate = false;
      this.changeFn(this.item.getRawValue());
    }

  }



}
