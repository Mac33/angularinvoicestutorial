import { Component, KeyValueDiffers, OnInit, ViewChild } from '@angular/core';
import {ElementRef} from '@angular/core';
import { ControlValueAccessor,  NG_VALUE_ACCESSOR, UntypedFormBuilder, UntypedFormGroup } from '@angular/forms';


@Component({
  selector: 'app-decimal-input',
  templateUrl: './decimal-input.component.html',
  styleUrls: ['./decimal-input.component.scss'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: DecimalInputComponent,
      multi: true
    }]
})
export class DecimalInputComponent implements  ControlValueAccessor {

  inClick :boolean = false;
  currentMask:string =  "separator.2"
  form: UntypedFormGroup;
  @ViewChild('decimalInput') el!:ElementRef;

  private changeFn: (value: number) => void = (value: number) => { };

  constructor(private fb: UntypedFormBuilder)  {
    this.form = fb.group(
      {
        decimalInputControl:'',
      }
    )
  }

  writeValue(obj: any): void {
    this.form.controls["decimalInputControl"]?.setValue(obj);
  }

  registerOnChange(fn: (value: number) => void): void {
    this.changeFn = fn;
  }

  registerOnTouched(fn: any): void {
  }

  setDisabledState?(isDisabled: boolean): void {
  }

  onValueChanged(e:Event)
  {
    this.changeFn(+this.el.nativeElement.value);
  }

  onMouseDownButtonDownEvent(e:Event){
    this.onClick("-");
  }

  onMouseDownButtonUpEvent(e:Event){
    this.onClick("+");
   }


  onClick(operation:string) {

    if(!this.inClick)
    {
      this.inClick = true;

      if(this.el.nativeElement.value == "") {
        this.el.nativeElement.value = 0;
      }

      let position : number =  this.el.nativeElement.selectionStart;
      let value : number = this.el.nativeElement.value as number;
      let totalLenght : number = (this.el.nativeElement.value as string).length
      let decimalPosition : number = (this.el.nativeElement.value as string).indexOf(".");
      let multiplicator : number = 1

      if (position == 0) {
          position = 1;
      }

      if (decimalPosition > 0) {
        multiplicator = (totalLenght - position) - (totalLenght-decimalPosition)
        if (decimalPosition < position)
        {
          multiplicator = +multiplicator + 1;
        }
      }else{
        multiplicator = totalLenght - position;
      }

      multiplicator = Math.pow(10,multiplicator);
      if (operation == "+")
      {
        value = +value + (+multiplicator);
      }
      if (operation == "-")
      {
        value = +value - (+multiplicator);
      }

      let strValue : string = (Math.round(value * 1e12) / 1e12).toString()

      // In "separator.2" mask does not work this format. And on "0*.00" this work but does not work negative numbers.
      // if (decimalPosition > 0 && position > decimalPosition )  {
      //   if (strValue.length + 1 == totalLenght)
      //   {
      //      strValue = strValue + "0";
      //   }
      //   if (strValue.length + 3 == totalLenght)
      //   {
      //      strValue = strValue + ".00";
      //   }

      //   if (strValue.length + 2 == totalLenght)
      //   {
      //      strValue = strValue + ".0";
      //   }
      // }

      if(strValue.length > totalLenght )
      {
        position = position + 1;
      }

      if(strValue.length < totalLenght )
      {
        position = position - 1;
      }

      this.form.controls["decimalInputControl"].setValue(strValue);
      this.changeFn(+strValue);
      setTimeout(() => {
        this.el.nativeElement.focus()
        this.el.nativeElement.setSelectionRange(position,position);
        this.inClick = false;
      },50);
    }
  }
}
