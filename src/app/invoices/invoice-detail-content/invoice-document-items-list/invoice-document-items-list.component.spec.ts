import { CommonModule } from '@angular/common';
import { Component } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { UntypedFormBuilder, FormControl, UntypedFormGroup, FormsModule, ReactiveFormsModule } from '@angular/forms';
import { By } from '@angular/platform-browser';
import { InvoiceDocumentItem } from '../../services/invoice-document-item';
import { InvoiceDocumentItemComponent } from './invoice-document-item/invoice-document-item.component';

import { InvoiceDocumentItemsListComponent } from './invoice-document-items-list.component';


@Component({
  template: `
        <form [formGroup]="invoiceFormItems">
          <app-invoice-document-items-list formControlName ='items' ></app-invoice-document-items-list>
        </form>`
})
class TestHostComponent {


  invoiceFormItems: UntypedFormGroup

  constructor(fb: UntypedFormBuilder) {
    this.invoiceFormItems = fb.group({
      items: [{ value: [] }]
    });

  }

  setValue(values: InvoiceDocumentItem[]): void {
    this.invoiceFormItems.reset();
    this.invoiceFormItems.setValue({
      items: values
    });
  }
}

describe('InvoiceDocumentItemsListComponent', () => {
  let component: TestHostComponent;
  let fixture: ComponentFixture<TestHostComponent>;

  let items: Array<InvoiceDocumentItem> = [{
    id: 1,
    itemName: "Test A",
    amount: 1,
    unitPrice: 10,
    totalPrice: 10
  },
  {
    id: 2,
    itemName: "Test B",
    amount: 3,
    unitPrice: 3,
    totalPrice: 9
  }]


  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [TestHostComponent,
        InvoiceDocumentItemComponent,
        InvoiceDocumentItemsListComponent],
      imports: [ReactiveFormsModule,
        FormsModule,
        CommonModule],
    })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TestHostComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  fit('Should create', () => {
    expect(component).toBeTruthy();
  });

  fit('Check number of elements after binding', () => {
    var count = fixture.debugElement.queryAll(By.css('app-invoice-document-item')).length;
    expect(count).toBe(0);
    component.setValue(items);
    fixture.detectChanges();
    var count = fixture.debugElement.queryAll(By.css('app-invoice-document-item')).length;
    expect(count).toBe(2);
  });


  fit('Check number of elements after click to add button', () => {
     var count = fixture.debugElement.queryAll(By.css('app-invoice-document-item')).length;
     expect(count).toBe(0);
     let button = fixture.debugElement.query(By.css('.button-add'));
     button.nativeElement.click();
     fixture.detectChanges();
     count = fixture.debugElement.queryAll(By.css('app-invoice-document-item')).length;
     expect(count).toBe(1);
   });


});
