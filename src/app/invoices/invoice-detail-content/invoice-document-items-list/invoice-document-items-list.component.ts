import { Component } from '@angular/core';
import { ControlValueAccessor, UntypedFormArray, UntypedFormBuilder, UntypedFormControl, UntypedFormGroup, NG_VALUE_ACCESSOR } from '@angular/forms';
import { InvoiceDocumentItem } from '../../services/invoice-document-item';

@Component({
  selector: 'app-invoice-document-items-list',
  templateUrl: './invoice-document-items-list.component.html',
  styleUrls: ['./invoice-document-items-list.component.scss'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: InvoiceDocumentItemsListComponent,
      multi: true
    }]
})
export class InvoiceDocumentItemsListComponent implements ControlValueAccessor {

  form: UntypedFormGroup;

  get items() {
    return this.form.controls["items"] as UntypedFormArray;
  }

  private changeFn: (value: InvoiceDocumentItem[]) => void = (value: InvoiceDocumentItem[]) => { };



  constructor(private fb: UntypedFormBuilder) {
    this.form = this.fb.group({
      items: this.fb.array([])
    });

    this.items.valueChanges.subscribe(
      () => {
        this.changeFn(this.items.getRawValue());
      }
    )
  }




  writeValue(values: InvoiceDocumentItem[]): void {
    this.items?.clear();
    if (values) {
      Array.from(values).forEach(
        (item)=> this.items.push(new UntypedFormControl(item))
      )
    }
  }



  registerOnChange(fn: any): void {
    this.changeFn = fn
  }



  registerOnTouched(fn: any): void { }



  getItem(index: number): UntypedFormControl {
    return this.items.at(index) as UntypedFormControl;
  }



  onRemoveItem(item: InvoiceDocumentItem) {
    let removeIndex = this.items.value.findIndex((i: InvoiceDocumentItem) => i.id == item.id)
    this.items.removeAt(removeIndex);
  }



  addItem() {
    this.items.push(new UntypedFormControl({
      id: this.items.length + 1,
      itemName: "",
      amount: 1,
      unitPrice: 0,
      totalPrice: 0
    }))
  }

}





