import { ComponentFixture, TestBed } from '@angular/core/testing';

import { InvoicesMainPageComponent } from './invoices-main-page.component';

describe('InvoicesComponent', () => {
  let component: InvoicesMainPageComponent;
  let fixture: ComponentFixture<InvoicesMainPageComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ InvoicesMainPageComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(InvoicesMainPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
