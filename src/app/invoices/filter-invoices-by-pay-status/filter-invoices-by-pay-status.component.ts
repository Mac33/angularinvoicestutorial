import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { UntypedFormControl } from '@angular/forms';

@Component({
  selector: 'app-filter-invoices-by-pay-status',
  templateUrl: './filter-invoices-by-pay-status.component.html',
  styleUrls: ['./filter-invoices-by-pay-status.component.scss']
})
export class FilterInvoicesByPayStatusComponent implements OnInit {

  @Output() searchTypeChange = new EventEmitter<string>();
  @Input() currentSearchType = '';

  searchType!:UntypedFormControl;

  constructor() { }

  ngOnInit(): void {

    this.searchType = new UntypedFormControl(this.currentSearchType)

    this.searchType.valueChanges
     .subscribe(
        (term) => {
          this.searchTypeChange.emit(term);
          console.log(term);
        }
      );

  }
}

