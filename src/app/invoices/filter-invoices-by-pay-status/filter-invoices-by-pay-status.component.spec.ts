import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FilterInvoicesByPayStatusComponent } from './filter-invoices-by-pay-status.component';

describe('FilterInvoicesByPayStatusComponent', () => {
  let component: FilterInvoicesByPayStatusComponent;
  let fixture: ComponentFixture<FilterInvoicesByPayStatusComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FilterInvoicesByPayStatusComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FilterInvoicesByPayStatusComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
