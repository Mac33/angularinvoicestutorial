import { Component, OnInit } from '@angular/core';
import { UntypedFormBuilder, UntypedFormGroup } from '@angular/forms';
import { Store } from '@ngrx/store';

import { NgxSpinnerService } from 'ngx-spinner';
import { getCurrentUser } from '../login/state/login-selectors';

import { InvoiceDocument } from './services/invoice-document';
import { InvoicesService } from './services/invoices.service';
import { getInvoceSelector, InvoiceActions } from './state/actions';
import { InvoicesState } from './state/invoices.reducer';
import { TotalPrice } from './total-price/total-price';



@Component({
  selector: 'app-invoices',
  templateUrl: './invoices-main-page.component.html',
  styleUrls: ['./invoices-main-page.component.scss'],
})
export class InvoicesMainPageComponent implements OnInit {

  form: UntypedFormGroup;
  invoices: InvoiceDocument[] = [];

  currentSearchTerm = "";
  currentSearchType = "";
  userHasEditRights:boolean = false;

  constructor(
    private invoiceService: InvoicesService,
    private fb: UntypedFormBuilder,
    private spinnerService: NgxSpinnerService,
    private store: Store<InvoicesState>) {

    this.form = this.fb.group({
      totalPriceInfo: [{ paidSum: 0, unpaidSum: 0 }],
    }
    )

  }


  onChangeSerachTypeChanged(value: string) {
    this.currentSearchType = value;
    this.setSearchtype(this.currentSearchType)
    this.doSerch(this.currentSearchTerm, this.currentSearchType);
  }




  onSearch(term: string) {
    this.currentSearchTerm = term;
    this.setSearchTerm(this.currentSearchTerm);
    this.doSerch(this.currentSearchTerm, this.currentSearchType);
  }



  ngOnInit(): void {

    this.invoiceService.onUpdateInvoce.subscribe({
      next: (newInvoice) => {
        this.updateSummaryInfo(newInvoice);
      }
    })

    this.store.select(getCurrentUser).subscribe(
      user => {
        this.userHasEditRights = user.hasEditRight;
      }
    );

    this.store.select(getInvoceSelector).subscribe(
      state => {
        this.currentSearchTerm = state.searchTerm;
        this.currentSearchType = state.filterByPayStatus;
        this.doSerch(this.currentSearchTerm, this.currentSearchType);
      }
    )

    this.invoiceService.onAddInvoce.subscribe({
      next: () => {
        this.onSearch(this.currentSearchTerm);
      }
    })
  }



  updateSummaryInfo(newInvoice: InvoiceDocument) {
    let invoice = this.invoices.filter(i => i.id == newInvoice.id).shift();
    if (invoice) {
      invoice.isPaid = newInvoice.isPaid;
      invoice.totalSum = newInvoice.totalSum;
      this.recalcSummaryInfo()
    }
  }



  private recalcSummaryInfo() {

    let newTotalPrice: TotalPrice = { paidSum: 0, unpaidSum: 0 };

    for (var item of this.invoices) {
      if (item.isPaid) {
        newTotalPrice.paidSum = newTotalPrice.paidSum + item.totalSum;
      } else {
        newTotalPrice.unpaidSum = newTotalPrice.unpaidSum + item.totalSum;
      }
    }

    this.form.patchValue({
      totalPriceInfo: newTotalPrice,
    });

  }


  private doSerch(term: string, type: string) {
    this.spinnerService.show();
    this.invoiceService.search(term, type).subscribe({
      next: (items) => {
        this.invoices = items;
        this.recalcSummaryInfo();
        this.spinnerService.hide()
      }, error: (err) => {
        console.log(err)
      }
    })
  }


  private setSearchTerm(value: string) {
    this.store.dispatch(InvoiceActions.setSearchFilter({ searchTerm: value }))
  }


  private setSearchtype(value: string) {
    this.store.dispatch(InvoiceActions.setPaymentFilter({ filterByPayStatus: value }))
  }

}
