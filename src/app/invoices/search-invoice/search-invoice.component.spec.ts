import { CommonModule } from '@angular/common';
import { Component, DebugElement } from '@angular/core';
import { ComponentFixture, TestBed, tick, fakeAsync, flush, discardPeriodicTasks } from '@angular/core/testing';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { SearchInvoiceComponent } from './search-invoice.component';

@Component({
  template: `
     <app-search-invoice (searchTermsChange)="onSearch($event)" ></app-search-invoice>`
})
class TestHostComponent {
  testSearchTerm = '';
  onSearch(value: string) {
    this.testSearchTerm = value;
  }
}


describe('SearchInvoiceComponent', () => {
  let componentMain: SearchInvoiceComponent;
  let fixtureMain: ComponentFixture<SearchInvoiceComponent>;

  let componentTest: TestHostComponent;
  let fixtureTest: ComponentFixture<TestHostComponent>;


  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [
        CommonModule,
        ReactiveFormsModule,
        FormsModule],
      declarations: [
        SearchInvoiceComponent,
        TestHostComponent]
    })
      .compileComponents();
  });

  beforeEach(() => {
    fixtureMain = TestBed.createComponent(SearchInvoiceComponent);
    componentMain = fixtureMain.componentInstance;
    fixtureMain.detectChanges();

    fixtureTest = TestBed.createComponent(TestHostComponent);
    componentTest = fixtureTest.componentInstance;
    fixtureTest.detectChanges();
  });

  fit('Should create SearchInvoiceComponent', () => {
    expect(componentMain).toBeTruthy();
  });

  fit('Should create TestHostComponent.', () => {
    expect(componentTest).toBeTruthy();
  });


  fit('Test search SearchInvoiceComponent.', fakeAsync(async () => {

    let testValue = "Test string";
    spyOn(componentMain.searchTermsChange, 'emit');
    const inputElement = fixtureMain.nativeElement.querySelector('input');
    inputElement.value = testValue
    inputElement.dispatchEvent(new Event('input'));
    tick(501);
    expect(componentMain.searchTermsChange.emit).toHaveBeenCalledWith(testValue);
  }));


  fit('Test search TestHostComponent for less than 500ms.', fakeAsync(async () => {
    let testValue = "Test string";
    const inputElement = fixtureTest.nativeElement.querySelector('input');
    inputElement.value = testValue
    inputElement.dispatchEvent(new Event('input'));
    tick(450);
    expect(componentTest.testSearchTerm != testValue).toBeTrue();
    discardPeriodicTasks();
  }));


  fit('Test search TestHostComponent above 500ms.', fakeAsync(async () => {
    let testValue = "Test string";
    const inputElement = fixtureTest.nativeElement.querySelector('input');
    inputElement.value = testValue
    inputElement.dispatchEvent(new Event('input'));
    tick(501);
    expect(componentTest.testSearchTerm == testValue).toBeTrue();
  }));

});
