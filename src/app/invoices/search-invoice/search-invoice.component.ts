import { Component, OnInit,ChangeDetectionStrategy, Output, EventEmitter, Input } from '@angular/core';
import { UntypedFormControl } from '@angular/forms';

import { debounceTime, distinctUntilChanged } from 'rxjs';


@Component({
  selector: 'app-search-invoice',
  templateUrl: './search-invoice.component.html',
  styleUrls: ['./search-invoice.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class SearchInvoiceComponent implements OnInit {

  @Output() searchTermsChange = new EventEmitter<string>();
  @Input() currentInput = '';


  searchInput!:  UntypedFormControl;

  constructor() { }

  ngOnInit(): void {

    this.searchInput = new UntypedFormControl(this.currentInput);

    this.searchInput.valueChanges
      .pipe(
        debounceTime(500),
        distinctUntilChanged()
      )
      .subscribe(
        (term) => {
          this.searchTermsChange.emit(term);
        }
      );
  }

}
