import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SideBarInvoiceListItemComponent } from './side-bar-invoice-list-item.component';

describe('InvoiceNavItemComponent', () => {
  let component: SideBarInvoiceListItemComponent;
  let fixture: ComponentFixture<SideBarInvoiceListItemComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SideBarInvoiceListItemComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SideBarInvoiceListItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
