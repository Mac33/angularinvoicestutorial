import { ChangeDetectionStrategy, Component, Input, OnInit } from '@angular/core';

import { InvoiceDocument } from '../services/invoice-document';
import { InvoicesService } from '../services/invoices.service';


@Component({
  selector: 'app-side-bar-invoice-list-item',
  templateUrl: './side-bar-invoice-list-item.component.html',
  styleUrls: ['./side-bar-invoice-list-item.component.scss'],
})
export class SideBarInvoiceListItemComponent implements OnInit {


  @Input() invoice?: InvoiceDocument;

  constructor(private invoiceService: InvoicesService) { }


  ngOnInit(): void {
    //console.log("Draw SideBarInvoiceListItem " + this.invoice?.id)
    this.invoiceService.onUpdateInvoce.subscribe({
      next: (newInvoice) => {
        if (this.invoice) {
          if (newInvoice.id == this.invoice?.id) {
            this.invoice = {...newInvoice};
          }
        }
      }
    });

  }
}

