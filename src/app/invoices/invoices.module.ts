import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { InvoiceSidebarFilterPipe } from './pipes/invoice-sidebar-filter.pipe';
import { SideBarInvoiceListItemComponent } from './side-bar-invoice-list-item/side-bar-invoice-list-item.component';
import { InvoicesMainPageComponent } from './invoices-main-page.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from '../shared/shared.module';
import { InvoiceDocumnetRoutingModule } from './invoice-document-routing.module';
import { ActivatedRoute } from '@angular/router';
import { InvoiceDetailContentComponent } from './invoice-detail-content/invoice-content.component';
import { SearchInvoiceComponent } from './search-invoice/search-invoice.component';
import { TotalPriceComponent } from './total-price/total-price.component';
import { NgxSpinnerModule } from "ngx-spinner";
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { InvoiceDocumentItemComponent } from './invoice-detail-content/invoice-document-items-list/invoice-document-item/invoice-document-item.component';
import { InvoiceDocumentItemsListComponent } from './invoice-detail-content/invoice-document-items-list/invoice-document-items-list.component';
import { FilterInvoicesByPayStatusComponent } from './filter-invoices-by-pay-status/filter-invoices-by-pay-status.component';

import { MatInputModule } from '@angular/material/input';
import { StoreModule } from '@ngrx/store';
import { invoicesReducer } from './state/invoices.reducer';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatSelectModule } from '@angular/material/select';
import { MatIconModule } from '@angular/material/icon';
import { DecimalInputComponent } from './invoice-detail-content/invoice-document-items-list/invoice-document-item/decimal-input/decimal-input.component';
import { NgxMaskModule, IConfig  } from 'ngx-mask'


const maskConfig: Partial<IConfig> = {
  validation: false,
};


@NgModule({
  declarations: [
    InvoiceSidebarFilterPipe,
    InvoicesMainPageComponent,
    SideBarInvoiceListItemComponent,
    InvoiceDetailContentComponent,
    SearchInvoiceComponent,
    TotalPriceComponent,
    InvoiceDocumentItemComponent,
    InvoiceDocumentItemsListComponent,
    FilterInvoicesByPayStatusComponent,
    DecimalInputComponent
  ],
  imports: [
    MatIconModule,
    MatInputModule,
    MatSelectModule,
    MatFormFieldModule,
    CommonModule,
    BrowserAnimationsModule,
    FormsModule,
    ReactiveFormsModule,
    SharedModule,
    InvoiceDocumnetRoutingModule,
    NgxSpinnerModule,
    StoreModule.forFeature('invoices', invoicesReducer),
    NgxMaskModule.forRoot(maskConfig),
    ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class InvoicesModule {
  constructor(private route: ActivatedRoute) {
    this.route.params.subscribe(params => console.log("Routing:" + params));
  }
}

