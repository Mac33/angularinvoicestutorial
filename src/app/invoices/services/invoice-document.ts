import { InvoiceDocumentItem } from "./invoice-document-item";


export class InvoiceDocument {

  constructor(public id = 0,
    public invoiceNumber = "",
    public partnerName = "",
    public dateTime = new Date(),
    public totalSum = 0,
    public isPaid = false) { }



  items: InvoiceDocumentItem[] = [];



}
