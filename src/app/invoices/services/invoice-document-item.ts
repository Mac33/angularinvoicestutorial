
export interface InvoiceDocumentItem {
  id: number;
  itemName: string;
  amount: number;
  unitPrice: number;
  totalPrice: number;
}
