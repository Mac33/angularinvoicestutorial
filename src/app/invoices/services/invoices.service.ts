import { HttpClient, HttpErrorResponse, HttpHeaders, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { catchError, Observable, of, Subject, tap } from 'rxjs';
import { InvoiceDocument } from './invoice-document';
import { throwError } from 'rxjs';


@Injectable()
export class InvoicesService {

  onAddInvoce: Subject<InvoiceDocument> = new Subject();

  onUpdateInvoce: Subject<InvoiceDocument> = new Subject();

  private invoicesUrl = 'api/invoices';

  private httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
  };

  constructor(private http: HttpClient) { }



  updateInvoice(invoice: InvoiceDocument):Observable<any>
  {
    return this.http.put(this.invoicesUrl, invoice, this.httpOptions).pipe(
      tap(_ => {
        this.onUpdateInvoce.next({...invoice}),
        console.log(`updated invoice id=${invoice.invoiceNumber}` )}
         ),
      catchError(this.handleError));
  }



  addInvoice(invoice: InvoiceDocument) {
    return this.http.post<InvoiceDocument>(this.invoicesUrl, invoice, this.httpOptions).pipe(
      tap((newInvoice: InvoiceDocument) => {
        console.log("added Invoice w/");
        console.log({ newInvoice })
        this.onAddInvoce.next(newInvoice);
      }),
      catchError(this.handleError)
    )
  }



  search(invoiceNumber: string, paidCondition:string ): Observable<InvoiceDocument[]> {
    console.log("Searching: " + invoiceNumber);

    invoiceNumber = invoiceNumber.trim();

    let searchCondition = `${this.invoicesUrl}/?`
    if(invoiceNumber != "")
    {
      searchCondition = `${searchCondition}invoiceNumber=${invoiceNumber}`;
    }

    if(paidCondition == "paid")
    {
      searchCondition = `${searchCondition}&isPaid=true`;
    }

    if(paidCondition == "unpaid")
    {
      searchCondition = `${searchCondition}&isPaid=false`;
    }

    return this.http.get<InvoiceDocument[]>(searchCondition).pipe(
      tap(
        data => {
          console.log(data.length);
        }),
      catchError(this.handleError)
    );
  }



  getInvoice(id: string): Observable<InvoiceDocument> {
    const url = `${this.invoicesUrl}/${id}`;
    return this.http.get<InvoiceDocument>(url).pipe(
      catchError(this.handleErrors<InvoiceDocument>("getInvoices",undefined))
    ) ;
  }



  getInvoices(): Observable<InvoiceDocument[]> {
    return this.http.get<InvoiceDocument[]>(this.invoicesUrl + "/?").pipe(
      tap(data => {
        console.log(data.length);
      }),
      catchError(this.handleErrors<InvoiceDocument[]>("getInvoices",[]))
    );

  }



  private handleError(err: HttpErrorResponse): Observable<never> {
    return throwError(() => {
      console.log(err)
      err
    }
    );
  }


  private handleErrors<T>(operation = "operation", result?: T) {
    return (error: any): Observable<T> => {
      console.error(error);
      return of(result as T);
    };
  }


}
