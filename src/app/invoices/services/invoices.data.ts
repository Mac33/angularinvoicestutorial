import { Injectable } from '@angular/core';
import { InMemoryDbService } from 'angular-in-memory-web-api';

import { InvoiceDocument } from './invoice-document';
import { InvoiceDocumentItem } from './invoice-document-item';


@Injectable({
  providedIn: 'root',
})
export class InvoicesData implements InMemoryDbService {

  maxInvoices: number = 50;


  createDb() : { invoices: InvoiceDocument[] }  {
    const items: InvoiceDocument[] = this.generateInvoces(this.maxInvoices);
    return {invoices:items };
  }


  genId(invoiceDocument: InvoiceDocument[]): number {
    return invoiceDocument.length > 0 ? Math.max(...invoiceDocument.map(item => item.id)) + 1 : (this.maxInvoices + 1000);
}


  private generateInvoces(count: number): InvoiceDocument[] {
    console.log("Start - generate invoices");
    let items: InvoiceDocument[] = [];
    for (let i = 1; i < count; i++) {
      let invoice: InvoiceDocument = this.generateInvoce(i, "FA000" + i.toString());
      items.push(invoice);
    }
    console.log("End - generate invoices");
    return items;
  }


  generateInvoce(id: number, number: string): InvoiceDocument {
    let genName: string = this.randomText()
    let randomDate = this.getRandomDate(new Date('2020-01-01T01:57:45.271Z'), new Date('2022-02-12T01:57:45.271Z'));

    let item: InvoiceDocument = {
      invoiceNumber: number,
      partnerName: genName,
      dateTime: randomDate,
      totalSum: 0,
      isPaid: Math.random() < 0.5,
      id: id,
      items: this.generateInvoceItems()
    }

    for(let i of item.items){
      item.totalSum = item.totalSum + i.totalPrice;
   }
    return item;
  }


  generateInvoceItems(): InvoiceDocumentItem[] {

    let items : InvoiceDocumentItem[] = [];
    let num =  Math.floor(Math.random() * (5)) + 1;

    for (let i = 0; i < num; i++) {
      let item: InvoiceDocumentItem = this.generateInvoceItem(i);
      items.push(item);
    }
    return items;

  }


  generateInvoceItem(id:number): InvoiceDocumentItem
  {
    let genName: string = this.randomText()
    let price =  Math.round((Math.random() * 1000) * 1000) / 1000
    let item : InvoiceDocumentItem =
    {
        id: id,
        itemName: genName,
        amount:1,
        unitPrice: price,
        totalPrice : price,
    }
    return  item;
  }




  private getRandomDate(from: Date, to: Date) {
    const fromTime = from.getTime();
    const toTime = to.getTime();
    return new Date(fromTime + Math.random() * (toTime - fromTime));
  }



  private randomText(): string {
    var text = "";
    var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

    for (var i = 0; i < 5; i++)
      text += possible.charAt(Math.floor(Math.random() * possible.length));

    return text;
  }



}
