import { Component, forwardRef, Input, OnInit } from '@angular/core';
import { ControlValueAccessor, UntypedFormBuilder,  NG_VALUE_ACCESSOR } from '@angular/forms';
import { TotalPrice } from './total-price';

@Component({
  selector: 'app-total-price',
  templateUrl: './total-price.component.html',
  styleUrls: ['./total-price.component.scss'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      multi: true,
      useExisting: TotalPriceComponent
    }]
})
export class TotalPriceComponent implements  ControlValueAccessor {

  sumForm = this.fb.group({
    paidSum: [0],
    unpaidSum: [0]
  });


  constructor(private fb: UntypedFormBuilder) { }



  writeValue(obj: TotalPrice): void {
      this.sumForm.setValue(obj);
  }



  registerOnChange(fn: any): void {
  }



  registerOnTouched(fn: any): void {
  }




}
