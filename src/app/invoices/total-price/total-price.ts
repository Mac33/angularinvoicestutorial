export interface TotalPrice{
  paidSum: number;
  unpaidSum: number;
}
