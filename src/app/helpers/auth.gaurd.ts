import { Injectable } from '@angular/core';
import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Actions, ofType } from '@ngrx/effects';
import { ActionCreator, Creator, Store } from '@ngrx/store';
import { map, take } from 'rxjs';
import { logInFromCookies, logInFromCookiesFailed, setCurrentUserSucess } from '../login/state/actions/login-actions';
import { State } from '../state/app.state';


@Injectable({ providedIn: 'root' })
export class AuthGuard implements CanActivate {
  constructor(
    private router: Router,
    private store: Store<State>,
    private actions$: Actions,
  ) { }

  async canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Promise<boolean> {

    const result = this.waitForResultAction(this.actions$, setCurrentUserSucess, logInFromCookiesFailed )
    this.store.dispatch(logInFromCookies());

    if(!await result)
    {
      this.router.navigate(['/login']);
    }
    return !!await result
}


 private waitForResultAction(
    action$: Actions,
    successAction: ActionCreator<string, Creator>,
    failureAction: ActionCreator<string, Creator>): Promise<boolean | undefined> {
    return action$.pipe(
      ofType(
        successAction,
        failureAction
      ),
      map((obj: object) =>
      {
        let action:{ type: string } = <{ type: string }>obj;
        return action.type === successAction.type;
      }),

      take(1)
    ).toPromise();
  }


}

