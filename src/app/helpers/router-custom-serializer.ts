import { InteractivityChecker } from '@angular/cdk/a11y';
import { Params, RouterStateSnapshot } from '@angular/router';
import { RouterStateSerializer } from '@ngrx/router-store';

export interface RouterStateUrl {
  url: string;
  params: Params;
  queryParams: Params;
  id:number;
  newInvoice:boolean;
}

export class RouterCustomSerializer implements RouterStateSerializer<RouterStateUrl> {

  serialize(routerState: RouterStateSnapshot): RouterStateUrl {
    let route = routerState.root;

    while (route.firstChild) {
      route = route.firstChild;
    }

    const {
      url,
      root: { queryParams },
    } = routerState;
    const { params } = route;

    let id = -1;

    if(params['id']!=undefined )
    {
        id=params['id'];
    }

    let newInvoice = false;
    if(url.endsWith('/new'))
    {
      newInvoice = true;
    }
    return { url, params, queryParams, id,newInvoice};
  }
}
