import { getCurrencySymbol } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { UntypedFormBuilder, UntypedFormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Store } from '@ngrx/store';
import { logIn } from '../state/actions/login-actions';
import { getCurrentUser, getLoginError } from '../state/login-selectors';
import {State} from '../../state/app.state'




@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  form: UntypedFormGroup;
  loading = false;
  wrongPassword = false;
  wrongPasswordMsg = '';

  constructor(
    private formBuilder: UntypedFormBuilder,
    private router: Router,
    private store: Store<State>) {

    this.form = this.formBuilder.group({
      username: ['', Validators.required],
      password: ['', Validators.required]
    });

    this.form.valueChanges.subscribe(
      () => {
        this.wrongPassword = false;
      })
  }


  ngOnInit(): void {

    this.store.select(getCurrentUser).subscribe(
      (user) => {
          if (user && user?.id > 0) {
          this.router.navigateByUrl('/invoices/new');
        }
        this.loading = false;
      }
    )


    this.store.select(getLoginError).subscribe(
      errMsg=>{
        if(errMsg)
        {
          this.wrongPassword = true;
          this.loading = false;
          this.wrongPasswordMsg = errMsg;
        }
      }
    )

  }

  onSubmit() {

    if (this.form.invalid) {
      return;
    }

    this.wrongPassword = false;
    const userName = this.form.controls["username"].value;
    const password = this.form.controls["password"].value;
    this.loading = true;
    this.store.dispatch(logIn({name:userName ,
                              password: password}));

  }


}
