
import { User } from '../user';

import { createReducer, on } from '@ngrx/store';
import { setCurrentUser,loadUserFailure, logIn, logInUserFailure, logOut } from './actions/login-actions';



export interface UserState {
  currentUser: User;
  userError: string;
  loginError: string
};

const initialState: UserState = {
  currentUser: new User(),
  userError: '',
  loginError:''
};


export const LoginReducer = createReducer<UserState>(
  initialState,
  on(setCurrentUser, (state, action): UserState =>{
    localStorage.setItem('user', JSON.stringify({id:action.currentUser.id}));
    return {
      ...state,
      currentUser: action.currentUser,
      loginError: '',
      userError: ''
    }
  } ),

  on(loadUserFailure,(state, action): UserState =>{
    return{
      ...state,
      currentUser: new User(),
      userError: action.error}
  } ),
  on(logOut,(state): UserState =>{
    localStorage.setItem('user', JSON.stringify({id:new User().id}));
    return{
      ...state,
      currentUser: new User(),
      loginError: '',
      userError: '' }
  } ),
  on(logInUserFailure,(state, action): UserState =>{
    return{
      ...state,
      currentUser: new User(),
      loginError: action.error}
  } ),
  on(logIn, (state, action): UserState => {
    return {
      ...state,
      loginError: ''
    };

  }),
);
