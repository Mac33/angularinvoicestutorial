import { Injectable } from '@angular/core';

import { map, catchError, concatMap, tap, switchMap } from 'rxjs/operators';
import { Observable, of } from 'rxjs';
import { LoginService } from '../services/login.service';

/* NgRx */
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { LoginActions } from './actions';
import { Router } from '@angular/router';
import { select, Store } from '@ngrx/store';
import { State } from '../../state/app.state'
import { User } from '../user';
import { getCurrentUser } from './login-selectors';


@Injectable()
export class LoginEffects {

  constructor(
    private actions$: Actions,
    private loginService: LoginService,
    private router: Router,
    private store: Store<State>) { }

  login$ = createEffect(() => {
    return this.actions$
      .pipe(
        ofType(LoginActions.logIn),
        concatMap(
          action =>
            this.loginService.login(action.name, action.password)
              .pipe(
                map(user => LoginActions.loadFullUserProfile({ id: user.id })),
                catchError(error => of(LoginActions.logInUserFailure({ error: "Nesprávne meno s alebo heslo používateľa" })))
              )
        )
      );
  });


  logInFromCookies$ = createEffect(() => {
    return this.actions$
      .pipe(
        ofType(LoginActions.logInFromCookies),
        switchMap(() => {
          const savedUser = localStorage.getItem("user")
          if (savedUser != null) {
            let userFromCookie = JSON.parse(savedUser);
            if (userFromCookie.id > 0) {
              let currentUser = this.getValue(this.store.pipe(select(getCurrentUser)));
              if (currentUser.id == userFromCookie.id) {
                return of(LoginActions.setCurrentUserSucess());
              } else {
                return of(LoginActions.loadFullUserProfile({ id: userFromCookie.id }));
              }
            }
          }
          return of(LoginActions.logInFromCookiesFailed());
        })
      );
  });

  loginOut$ = createEffect(() => {
    return this.actions$
      .pipe(
        ofType(LoginActions.logOut),
        map(
          () => {
            localStorage.removeItem('user');
            this.router.navigate(['/login']);
            return LoginActions.logOutSucess();
          }
        )
      );
  });


  loadFullUserProfile$ = createEffect(() => {
    return this.actions$
      .pipe(
        ofType(LoginActions.loadFullUserProfile),
        concatMap(action =>
          this.loginService.getById(action.id).pipe(
            map(
              (user) => {
                console.log("SetCurrentUser ", user);
                localStorage.setItem('user', JSON.stringify({ id: user.id }));
                return LoginActions.setCurrentUser({ currentUser: user })
              }
            ),
            catchError(error => of(LoginActions.loadUserFailure({ error }))))
        )
      )
  });


  setCurrentUser$ = createEffect(() => {
    return this.actions$
      .pipe(
        ofType(LoginActions.setCurrentUser),
        map(
          () => {
           return LoginActions.setCurrentUserSucess();
          }
        )
      );
  });



  private getValue(obj: Observable<any>) {
    let value: any;
    obj.subscribe(v => value = v);
    return value;
  }
}
