import { createFeatureSelector, createSelector } from '@ngrx/store';
import { UserState } from './login.reducer';

export const selectorFeatureName = 'login';

// Selector functions
const getUserFeatureState = createFeatureSelector<UserState>(selectorFeatureName);

export const getCurrentUser = createSelector(
  getUserFeatureState,
  state => state.currentUser
);

export const getLoginError = createSelector(
  getUserFeatureState,
  state => state.loginError
);

