import { User } from '../../user';
import { createAction, props } from '@ngrx/store';



export const logInUserFailure = createAction(
  '[Login Page] Login fail',
  props<{ error: string }>()
);


export const loadUserFailure = createAction(
  '[User API] Load fail',
  props<{ error: string }>()
);

export const setCurrentUser = createAction(
  '[Login Page] Set Current User',
  props<{ currentUser: User }>()
);

export const setCurrentUserSucess = createAction(
  '[Login] Set Current User sucess'
);

export const loadFullUserProfile = createAction(
  '[Login Page] Load full user profile',
  props<{ id: number }>()
);

export const checkLocalStorage = createAction(
  '[Login Page] Load full user profile'
);

export const logIn = createAction(
  '[Login Page] LogIn user',
  props<{ name: string,
          password: string }>()
);

export const logOut = createAction(
  '[Main Page] LogOut user'
);

export const logInFromCookiesSucess = createAction(
  '[Login Page] LogIn from cookies sucess'
);

export const logInFromCookiesFailed = createAction(
  '[Login Page] LogIn from cookies failed'
);

export const logOutSucess = createAction(
  '[Main Page] Load from coc sucess'
);

export const logInFromCookies = createAction(
  '[Main Page] LogIn from cookies'
);
