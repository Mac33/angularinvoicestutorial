import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { LogoutDialogData } from './logout.dialog.data';

@Component({
  selector: 'app-logout-msg',
  templateUrl: './logout-msg.component.html',
  styleUrls: ['./logout-msg.component.scss']
})
export class LogoutMsgComponent  {

  constructor(
    public dialogRef: MatDialogRef<LogoutMsgComponent>,
    @Inject(MAT_DIALOG_DATA) public data: LogoutDialogData,
  ) {}


  onNoClick(): void {
    this.dialogRef.close(false);
  }


}
