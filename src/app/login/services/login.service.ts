import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject, Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { User } from '../user';

@Injectable()
export class LoginService {

  static userUrl = "http://localhost:4200"
  private userSubject: BehaviorSubject<User>;

  constructor(
    private router: Router,
    private http: HttpClient
  ) {

    this.userSubject = new BehaviorSubject<User>(new User());
    const savedUser = localStorage.getItem("user")
    if (savedUser!=null){
     this.userSubject.next(JSON.parse(savedUser));
    }

  }

  public get userValue(): User {
    return this.userSubject.value;
  }

  login(username: string, password: string) {
    return this.http.post<User>(`${LoginService.userUrl}/users/authenticate`, { username, password })
      .pipe(map(user => {
        this.userSubject.next(user);
        return user;
      }));
  }

  logout() {
    this.router.navigate(['/login']);
  }


  getById(id: number) {
    return this.http.get<User>(`${LoginService.userUrl}/users/${id}`);
  }


}
