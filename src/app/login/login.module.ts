import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MatDialogModule } from '@angular/material/dialog';
import { MatInputModule } from '@angular/material/input';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatListModule } from '@angular/material/list';
import { MatSelectModule } from '@angular/material/select';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatCardModule } from '@angular/material/card';
import { MatTableModule } from '@angular/material/table';
import { MatButtonModule } from '@angular/material/button';
import {MatToolbarModule} from '@angular/material/toolbar';

import { LoginComponent } from './login/login.component';
import { LoginRoutingModule } from './login-routing.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { JwtInterceptor } from './services/jwt.interceptor';

import { FakeBackendProvider } from './services/fake-backed';
import { ErrorInterceptor } from './services/error.interceptor';

import { EffectsModule } from '@ngrx/effects';
import { LoginEffects } from './state/login-efects';
import { LoginReducer } from './state/login.reducer';

import { StoreModule } from '@ngrx/store';
import { selectorFeatureName } from './state/login-selectors';
import { LogoutMsgComponent } from './dialog/logout-msg/logout-msg.component';
import { LoginService } from './services/login.service';

@NgModule({
  declarations: [LoginComponent, LogoutMsgComponent],
  imports: [
    FormsModule,
    MatButtonModule,
    MatToolbarModule,
    MatIconModule,
    MatListModule,
    MatSelectModule,
    MatSidenavModule,
    MatCardModule,
    MatTableModule,
    MatInputModule,
    MatDialogModule,
    MatFormFieldModule,
    CommonModule,
    LoginRoutingModule,
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    StoreModule.forFeature(selectorFeatureName , LoginReducer),
    EffectsModule.forFeature([LoginEffects])
  ],
  providers: [
    { provide: HTTP_INTERCEPTORS, useClass: JwtInterceptor, multi: true },
    { provide: HTTP_INTERCEPTORS, useClass: ErrorInterceptor, multi: true },
    LoginService,
    FakeBackendProvider
],
})
export class LoginModule {
  constructor() {}
}
