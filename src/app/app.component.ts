import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Router, Event as NavigationEvent, NavigationStart, NavigationEnd } from '@angular/router';
import { Store } from '@ngrx/store';
import { getInvoceSelector } from './invoices/state/actions';
import { State } from './invoices/state/invoices.reducer';
import { LogoutMsgComponent } from './login/dialog/logout-msg/logout-msg.component';
import { logOut } from './login/state/actions/login-actions';
import { getCurrentUser } from './login/state/login-selectors';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']


})
export class AppComponent {

  isShowingLoginPage = false;
  title = 'angular-invoice-page-tutorial';
  userName = '';
  navLink = 'new';

  constructor(
    private router: Router,
    private store: Store<State>,
    public dialog: MatDialog) {


    this.store.select(getInvoceSelector).subscribe(
      state => {
        if(state.filterByPayStatus)
        if (state.lastInvoiceNumber > 0) {
          this.navLink = state.lastInvoiceNumber.toString();
        } else {
          this.navLink = 'new';
        }
      }
    )


    this.store.select(getCurrentUser).subscribe(
      user => {
        this.userName = user.username;
      }
    )

    this.router.events.subscribe(
      (event: NavigationEvent) => {

        if (event instanceof NavigationEnd) {
          this.isShowingLoginPage = event.url.startsWith('/login') || event.urlAfterRedirects.startsWith('/login');
        }
      }
    )

  }

  onLogOut() {
    const dialogRef = this.dialog.open(LogoutMsgComponent, { data: { userName: this.userName }, panelClass: 'custom-log-out-modal-box' });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.store.dispatch(logOut());
      }
    });
  }


}
