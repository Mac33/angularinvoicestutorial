import { UserState } from '../login/state/login.reducer';

export interface State {
  user: UserState;
}
