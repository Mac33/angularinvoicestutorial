import * as fromRouter from '@ngrx/router-store';
import { createSelector } from '@ngrx/store';
import { StoreRootState } from './store-root.state';


export const getRouterState = function(state: StoreRootState) { return state.router};

export const getCurrentRouteState = createSelector(
  getRouterState,
  function(state: fromRouter.RouterReducerState){
    return state.state;
  }
);
