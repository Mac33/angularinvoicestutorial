import { Injectable } from '@angular/core';

import { map, catchError, concatMap, tap, switchMap } from 'rxjs/operators';
import { Observable, of } from 'rxjs';

/* NgRx */
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { Router } from '@angular/router';
import { select, Store } from '@ngrx/store';
import { routerNavigatedAction } from '@ngrx/router-store';
import { State } from '../state/app.state'
import {InvoiceActions } from '../invoices/state/actions'
import { RouterStateUrl } from '../helpers/router-custom-serializer'

@Injectable()
export class RootEffects {

  constructor(
    private actions$: Actions,
    private router: Router,
    private store: Store<State>) { }

  invoiceRoot$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType(routerNavigatedAction),
        tap(({ payload }) => {
          let routerState =(payload.routerState as unknown as RouterStateUrl);
          let id = 0;
          if(!routerState.newInvoice)
          {
            id = routerState.id
          }
          if(id >= 0) {
            this.store.dispatch(InvoiceActions.setLastInvoce({ invoiceNumber: id }));
          }
        })
      ),
    { dispatch: false }
  );

}
