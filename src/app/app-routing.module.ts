import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuard } from './helpers/auth.gaurd';
import { HomeComponent } from './home/home.component';


const loginModule = () => import('./login/login.module').then(x => x.LoginModule);


const routes: Routes = [
  { path: '',  component: HomeComponent , canActivate: [AuthGuard]  },
  { path: 'login', loadChildren: loginModule },
  { path: 'home', component: HomeComponent , canActivate: [AuthGuard]  },
  { path: '**',   redirectTo: '/invoices/new', pathMatch: 'full'},
];


@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
