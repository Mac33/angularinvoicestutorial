import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SharedRoutingModule } from './shared-routing.module';
import { ErrorMessageComponent } from './components/error-message/error-message.component';
import { TwoDecimalPlacesPipe } from './pipes/two-decimal-places.pipe';
import { MarkAsteriskDirective } from "./directives/mark-asterisk.directive";
import { CurrencySignPipe } from './pipes/currency-sign.pipe';
import { LockContentDirective } from './directives/lock-content.directive';
import { StoreModule } from '@ngrx/store';
import { selectorFeatureName } from '../login/state/login-selectors';
import { LoginReducer } from '../login/state/login.reducer';
import { NumericLabelComponent } from './components/numeric-label/numeric-label.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

@NgModule({
  declarations: [
    ErrorMessageComponent,
    TwoDecimalPlacesPipe,
    MarkAsteriskDirective,
    CurrencySignPipe,
    LockContentDirective,
    NumericLabelComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    SharedRoutingModule,
    StoreModule.forFeature(selectorFeatureName , LoginReducer),
  ],
  exports: [
    LockContentDirective,
    ErrorMessageComponent,
    TwoDecimalPlacesPipe,
    MarkAsteriskDirective,
    CurrencySignPipe,
    NumericLabelComponent ]
})
export class SharedModule { }

