import { Component } from '@angular/core';
import { ControlValueAccessor, UntypedFormControl, NG_VALUE_ACCESSOR } from '@angular/forms';
import { CurrencySignPipe } from '../../pipes/currency-sign.pipe';
import { TwoDecimalPlacesPipe } from '../../pipes/two-decimal-places.pipe';

@Component({
  selector: 'app-numeric-label',
  templateUrl: './numeric-label.component.html',
  styleUrls: ['./numeric-label.component.scss'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: NumericLabelComponent,
      multi: true
    }]
})
export class NumericLabelComponent implements  ControlValueAccessor {

  textField = new UntypedFormControl({value:0, disabled: true});

  constructor() {}

  writeValue(obj: number): void {
      let formatedValue = new TwoDecimalPlacesPipe().transform(obj)
      formatedValue = new CurrencySignPipe().transform(formatedValue)
      this.textField.setValue(formatedValue);
  }


  registerOnChange(fn: any): void {
  }


  registerOnTouched(fn: any): void {
  }

}
