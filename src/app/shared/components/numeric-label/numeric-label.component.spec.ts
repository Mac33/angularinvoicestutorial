import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NumericLabelComponent } from './numeric-label.component';

describe('NumericLabelComponent', () => {
  let component: NumericLabelComponent;
  let fixture: ComponentFixture<NumericLabelComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ NumericLabelComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(NumericLabelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
