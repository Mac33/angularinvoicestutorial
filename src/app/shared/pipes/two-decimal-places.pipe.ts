import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'twoDecimalPlaces',
  pure:true
})
export class TwoDecimalPlacesPipe implements PipeTransform {

  transform(value: number): string {

    if(value == undefined || value == null )
    {
      return "";
    }

    let strValue:string = value.toString();

    if(strValue.startsWith("0") && strValue.length > 1 && (strValue.indexOf(".") == -1 || strValue.indexOf(".") > 1 ) )
    {
      strValue = strValue.substring(1,strValue.length);
    }

    if(strValue.indexOf(".") == -1)
    {
        return strValue + ".00";
    }

    let position = strValue.indexOf(".");

    if(position == strValue.length - 2)
    {
      return   strValue + "0";
    }

    strValue = strValue.substring(0, position + 3);

    return strValue;
  }



}
