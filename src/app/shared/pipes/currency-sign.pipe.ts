import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'currencySign',
  pure: true
})
export class CurrencySignPipe implements PipeTransform {

  transform(value: string, sign = "€"): string {
    if (value) {
      return value + " " + sign;

    } else {
      return "";
    }
  }

}
