import { Directive, ElementRef } from '@angular/core';
import { Store } from '@ngrx/store';
import { getCurrentUser } from 'src/app/login/state/login-selectors';
import { State } from 'src/app/state/app.state';


@Directive({
  selector: '[lockContent]'
})
export class LockContentDirective {

  constructor(
    private store: Store<State>,
    el: ElementRef) {
      el.nativeElement.style = 'pointer-events:none; color:gray; opacity: 0.4;';
      this.store.select(getCurrentUser).subscribe(
        user => {
          if(user.hasEditRight){
            el.nativeElement.style = '';
          }
        }
      );
  }

}
